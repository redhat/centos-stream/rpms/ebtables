From 22476c0758b930b972c5397ee3ba1bef5ccf6223 Mon Sep 17 00:00:00 2001
From: Jan Engelhardt <jengelh@inai.de>
Date: Wed, 27 Jun 2018 11:50:41 +0200
Subject: [PATCH] extensions: use __attribute__((constructor)) for
 autoregistration

The ebtables initialization is easier, and, judging from the "static"
recipe in Makefile, that calling ebt_*_register ahead of main is
safe.

This means that a static build won't need the pseudomain hack,
and that -nostartfiles can also go away.

Signed-off-by: Florian Westphal <fw@strlen.de>
Signed-off-by: Phil Sutter <psutter@redhat.com>
---
 Makefile                    | 34 +---------------------------------
 extensions/Makefile         |  4 ++--
 extensions/ebt_802_3.c      |  2 +-
 extensions/ebt_among.c      |  2 +-
 extensions/ebt_arp.c        |  2 +-
 extensions/ebt_arpreply.c   |  2 +-
 extensions/ebt_ip.c         |  2 +-
 extensions/ebt_ip6.c        |  2 +-
 extensions/ebt_limit.c      |  2 +-
 extensions/ebt_log.c        |  2 +-
 extensions/ebt_mark.c       |  2 +-
 extensions/ebt_mark_m.c     |  2 +-
 extensions/ebt_nat.c        |  2 +-
 extensions/ebt_nflog.c      |  2 +-
 extensions/ebt_pkttype.c    |  2 +-
 extensions/ebt_redirect.c   |  2 +-
 extensions/ebt_standard.c   |  2 +-
 extensions/ebt_stp.c        |  2 +-
 extensions/ebt_string.c     |  2 +-
 extensions/ebt_ulog.c       |  2 +-
 extensions/ebt_vlan.c       |  2 +-
 extensions/ebtable_broute.c |  2 +-
 extensions/ebtable_filter.c |  2 +-
 extensions/ebtable_nat.c    |  2 +-
 include/ebtables_u.h        |  2 ++
 25 files changed, 27 insertions(+), 57 deletions(-)

diff --git a/Makefile b/Makefile
index 7c70db0267983..4d7b10f4916d6 100644
--- a/Makefile
+++ b/Makefile
@@ -116,39 +116,7 @@ daemon: ebtablesd ebtablesu
 # a little scripting for a static binary, making one for ebtables-restore
 # should be completely analogous
 static: extensions/ebt_*.c extensions/ebtable_*.c ebtables.c communication.c ebtables-standalone.c getethertype.c libebtc.c useful_functions.c
-	cp ebtables-standalone.c ebtables-standalone.c_ ; \
-	cp include/ebtables_u.h include/ebtables_u.h_ ; \
-	sed "s/ main(/ pseudomain(/" ebtables-standalone.c > ebtables-standalone.c__ ; \
-	mv ebtables-standalone.c__ ebtables-standalone.c ; \
-	printf "\nint main(int argc, char *argv[])\n{\n "  >> ebtables-standalone.c ; \
-	for arg in $(EXT_FUNC) \
-	; do \
-	sed s/_init/_$${arg}_init/ extensions/ebt_$${arg}.c > extensions/ebt_$${arg}.c_ ; \
-	mv extensions/ebt_$${arg}.c_ extensions/ebt_$${arg}.c ; \
-	printf "\t%s();\n" _$${arg}_init >> ebtables-standalone.c ; \
-	printf "extern void %s();\n" _$${arg}_init >> include/ebtables_u.h ; \
-	done ; \
-	for arg in $(EXT_TABLES) \
-	; do \
-	sed s/_init/_t_$${arg}_init/ extensions/ebtable_$${arg}.c > extensions/ebtable_$${arg}.c_ ; \
-	mv extensions/ebtable_$${arg}.c_ extensions/ebtable_$${arg}.c ; \
-	printf "\t%s();\n" _t_$${arg}_init >> ebtables-standalone.c ; \
-	printf "extern void %s();\n" _t_$${arg}_init >> include/ebtables_u.h ; \
-	done ; \
-	printf "\n\tpseudomain(argc, argv);\n\treturn 0;\n}\n" >> ebtables-standalone.c ;\
-	$(CC) $(CFLAGS) $(LDFLAGS) $(PROGSPECS) -o $@ $^ -I$(KERNEL_INCLUDES) -Iinclude ; \
-	for arg in $(EXT_FUNC) \
-	; do \
-	sed "s/ .*_init/ _init/" extensions/ebt_$${arg}.c > extensions/ebt_$${arg}.c_ ; \
-	mv extensions/ebt_$${arg}.c_ extensions/ebt_$${arg}.c ; \
-	done ; \
-	for arg in $(EXT_TABLES) \
-	; do \
-	sed "s/ .*_init/ _init/" extensions/ebtable_$${arg}.c > extensions/ebtable_$${arg}.c_ ; \
-	mv extensions/ebtable_$${arg}.c_ extensions/ebtable_$${arg}.c ; \
-	done ; \
-	mv ebtables-standalone.c_ ebtables-standalone.c ; \
-	mv include/ebtables_u.h_ include/ebtables_u.h
+	$(CC) $(CFLAGS) $(LDFLAGS) $(PROGSPECS) -o $@ $^ -I$(KERNEL_INCLUDES) -Iinclude
 
 tmp1:=$(shell printf $(BINDIR) | sed 's/\//\\\//g')
 tmp2:=$(shell printf $(SYSCONFIGDIR) | sed 's/\//\\\//g')
diff --git a/extensions/Makefile b/extensions/Makefile
index 60a70a2298357..daa11fce36e5e 100644
--- a/extensions/Makefile
+++ b/extensions/Makefile
@@ -11,13 +11,13 @@ EXT_LIBSI+=$(foreach T,$(EXT_FUNC), -lebt_$(T))
 EXT_LIBSI+=$(foreach T,$(EXT_TABLES), -lebtable_$(T))
 
 extensions/ebt_%.so: extensions/ebt_%.o
-	$(CC) $(LDFLAGS) -shared -o $@ -lc $< -nostartfiles
+	$(CC) $(LDFLAGS) -shared -o $@ -lc $<
 
 extensions/libebt_%.so: extensions/ebt_%.so
 	mv $< $@
 
 extensions/ebtable_%.so: extensions/ebtable_%.o
-	$(CC) $(LDFLAGS) -shared -o $@ -lc $< -nostartfiles
+	$(CC) $(LDFLAGS) -shared -o $@ -lc $<
 
 extensions/libebtable_%.so: extensions/ebtable_%.so
 	mv $< $@
diff --git a/extensions/ebt_802_3.c b/extensions/ebt_802_3.c
index 458484939231d..d70fd441e60db 100644
--- a/extensions/ebt_802_3.c
+++ b/extensions/ebt_802_3.c
@@ -141,7 +141,7 @@ static struct ebt_u_match _802_3_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&_802_3_match);
 }
diff --git a/extensions/ebt_among.c b/extensions/ebt_among.c
index e4fc5ac22a005..b1560e8f09e8d 100644
--- a/extensions/ebt_among.c
+++ b/extensions/ebt_among.c
@@ -491,7 +491,7 @@ static struct ebt_u_match among_match = {
 	.extra_ops 	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&among_match);
 }
diff --git a/extensions/ebt_arp.c b/extensions/ebt_arp.c
index b2819553ab313..84b6e900eff62 100644
--- a/extensions/ebt_arp.c
+++ b/extensions/ebt_arp.c
@@ -362,7 +362,7 @@ static struct ebt_u_match arp_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&arp_match);
 }
diff --git a/extensions/ebt_arpreply.c b/extensions/ebt_arpreply.c
index 51eda66adbff3..399868bdd9059 100644
--- a/extensions/ebt_arpreply.c
+++ b/extensions/ebt_arpreply.c
@@ -133,7 +133,7 @@ static struct ebt_u_target arpreply_target =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_target(&arpreply_target);
 }
diff --git a/extensions/ebt_ip.c b/extensions/ebt_ip.c
index 1ffdb95f156df..faffade35f7f1 100644
--- a/extensions/ebt_ip.c
+++ b/extensions/ebt_ip.c
@@ -472,7 +472,7 @@ static struct ebt_u_match ip_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&ip_match);
 }
diff --git a/extensions/ebt_ip6.c b/extensions/ebt_ip6.c
index 347797b4afe18..17a4303177284 100644
--- a/extensions/ebt_ip6.c
+++ b/extensions/ebt_ip6.c
@@ -413,7 +413,7 @@ static struct ebt_u_match ip6_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&ip6_match);
 }
diff --git a/extensions/ebt_limit.c b/extensions/ebt_limit.c
index 2cbf4dee51fb4..1fe9d84ffd137 100644
--- a/extensions/ebt_limit.c
+++ b/extensions/ebt_limit.c
@@ -212,7 +212,7 @@ static struct ebt_u_match limit_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&limit_match);
 }
diff --git a/extensions/ebt_log.c b/extensions/ebt_log.c
index 97d50919d25ca..b5d32321948c8 100644
--- a/extensions/ebt_log.c
+++ b/extensions/ebt_log.c
@@ -217,7 +217,7 @@ static struct ebt_u_watcher log_watcher =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_watcher(&log_watcher);
 }
diff --git a/extensions/ebt_mark.c b/extensions/ebt_mark.c
index 4cf1378d5085c..b4f93b5960b6a 100644
--- a/extensions/ebt_mark.c
+++ b/extensions/ebt_mark.c
@@ -172,7 +172,7 @@ static struct ebt_u_target mark_target =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_target(&mark_target);
 }
diff --git a/extensions/ebt_mark_m.c b/extensions/ebt_mark_m.c
index 7561f059c0108..b6d11a2903bbe 100644
--- a/extensions/ebt_mark_m.c
+++ b/extensions/ebt_mark_m.c
@@ -121,7 +121,7 @@ static struct ebt_u_match mark_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&mark_match);
 }
diff --git a/extensions/ebt_nat.c b/extensions/ebt_nat.c
index 00d9cd4083247..fe7e9875498e0 100644
--- a/extensions/ebt_nat.c
+++ b/extensions/ebt_nat.c
@@ -231,7 +231,7 @@ static struct ebt_u_target dnat_target =
 	.extra_ops	= opts_d,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_target(&snat_target);
 	ebt_register_target(&dnat_target);
diff --git a/extensions/ebt_nflog.c b/extensions/ebt_nflog.c
index 405673a01f893..04c547d06cee0 100644
--- a/extensions/ebt_nflog.c
+++ b/extensions/ebt_nflog.c
@@ -166,7 +166,7 @@ static struct ebt_u_watcher nflog_watcher = {
 	.extra_ops = nflog_opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_watcher(&nflog_watcher);
 }
diff --git a/extensions/ebt_pkttype.c b/extensions/ebt_pkttype.c
index 486c85c3c3faf..bf578fcf98f92 100644
--- a/extensions/ebt_pkttype.c
+++ b/extensions/ebt_pkttype.c
@@ -125,7 +125,7 @@ static struct ebt_u_match pkttype_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&pkttype_match);
 }
diff --git a/extensions/ebt_redirect.c b/extensions/ebt_redirect.c
index 3f8227a917583..59fe818f7b205 100644
--- a/extensions/ebt_redirect.c
+++ b/extensions/ebt_redirect.c
@@ -108,7 +108,7 @@ static struct ebt_u_target redirect_target =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_target(&redirect_target);
 }
diff --git a/extensions/ebt_standard.c b/extensions/ebt_standard.c
index 81edead71a840..f3c33086bac53 100644
--- a/extensions/ebt_standard.c
+++ b/extensions/ebt_standard.c
@@ -84,7 +84,7 @@ static struct ebt_u_target standard =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_target(&standard);
 }
diff --git a/extensions/ebt_stp.c b/extensions/ebt_stp.c
index 5c5fc3334311d..311bc63d2cb0c 100644
--- a/extensions/ebt_stp.c
+++ b/extensions/ebt_stp.c
@@ -337,7 +337,7 @@ static struct ebt_u_match stp_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&stp_match);
 }
diff --git a/extensions/ebt_string.c b/extensions/ebt_string.c
index 7d24f8002f4d7..97fbe19eca54f 100644
--- a/extensions/ebt_string.c
+++ b/extensions/ebt_string.c
@@ -312,7 +312,7 @@ static struct ebt_u_match string_match =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&string_match);
 }
diff --git a/extensions/ebt_ulog.c b/extensions/ebt_ulog.c
index 54eec53f7069f..72a6c8b199b42 100644
--- a/extensions/ebt_ulog.c
+++ b/extensions/ebt_ulog.c
@@ -180,7 +180,7 @@ static struct ebt_u_watcher ulog_watcher =
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_watcher(&ulog_watcher);
 }
diff --git a/extensions/ebt_vlan.c b/extensions/ebt_vlan.c
index 0a37067b5ebde..0818d48e8521b 100644
--- a/extensions/ebt_vlan.c
+++ b/extensions/ebt_vlan.c
@@ -181,7 +181,7 @@ static struct ebt_u_match vlan_match = {
 	.extra_ops	= opts,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_match(&vlan_match);
 }
diff --git a/extensions/ebtable_broute.c b/extensions/ebtable_broute.c
index 5259355e2b01b..c106f0825a147 100644
--- a/extensions/ebtable_broute.c
+++ b/extensions/ebtable_broute.c
@@ -23,7 +23,7 @@ ebt_u_table table =
 	.help		= print_help,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_table(&table);
 }
diff --git a/extensions/ebtable_filter.c b/extensions/ebtable_filter.c
index e41fb84ffbf20..c0bf105d75986 100644
--- a/extensions/ebtable_filter.c
+++ b/extensions/ebtable_filter.c
@@ -29,7 +29,7 @@ static struct ebt_u_table table =
 	.help		= print_help,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_table(&table);
 }
diff --git a/extensions/ebtable_nat.c b/extensions/ebtable_nat.c
index b21c9ddaedd46..ee044823866c3 100644
--- a/extensions/ebtable_nat.c
+++ b/extensions/ebtable_nat.c
@@ -30,7 +30,7 @@ ebt_u_table table =
 	.help		= print_help,
 };
 
-void _init(void)
+static void _INIT(void)
 {
 	ebt_register_table(&table);
 }
diff --git a/include/ebtables_u.h b/include/ebtables_u.h
index 3235bf5967055..7f5968dc6f39d 100644
--- a/include/ebtables_u.h
+++ b/include/ebtables_u.h
@@ -44,6 +44,8 @@
 #define EBT_ALIGN(s) (((s) + (EBT_MIN_ALIGN-1)) & ~(EBT_MIN_ALIGN-1))
 #define ERRORMSG_MAXLEN 128
 
+#define _INIT __attribute__((constructor)) _init
+
 struct ebt_u_entries
 {
 	int policy;
-- 
2.21.0

